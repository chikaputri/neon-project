using System.Collections.Generic;

namespace LatihanCustomer.omni.Models
{
    public class CountModel
    {
        public string Value { get; set; }
        public int Total { get; set; }
    }
}