using LatihanCustomer.omni.Models;
using LatihanCustomer.omni.Services;

namespace LatihanCustomer.omni.Logics
{
    public class NasabahCoreLogic : IDisposable
    {
        private GlobalVariable _globalVariable;
        private INasabahService _nasabahService;

        public void Dispose()
        {
            Console.WriteLine("dispose nasabahlogic start");
        }

        public NasabahCoreLogic(GlobalVariable globalVariable, INasabahService nasabahService)
        {
            _globalVariable = globalVariable;
            _nasabahService = nasabahService;
        }

        public object GetListNasabah()
        {
            object objOut = new object();
            objOut = _nasabahService.GetListNasabah();

            return objOut;
        }

        public object GetListProfesi()
        {
            object objOut = _nasabahService.GetListProfesi();

            return objOut;
        }

        public object GetListKota()
        {
            object objOut = _nasabahService.GetListKota();

            return objOut;
        }

        public object GetListKodePos()
        {
            object objOut = _nasabahService.GetListKodePos();

            return objOut;
        }

        public object GetListKecamatan()
        {
            object objOut = _nasabahService.GetListKecamatan();

            return objOut;
        }

        public object GetListJenisKelamin()
        {
            object objOut = _nasabahService.GetListJenisKelamin();

            return objOut;
        }

        public object AddNewNasabah(NasabahModel data)
        {
            object objOut = new object();
            objOut = _nasabahService.AddNewNasabah(data);

            return objOut;
        }

        public object UpdateNasabah(NasabahModel data)
        {
            object objOut = new object();
            objOut = _nasabahService.UpdateNasabah(data);

            return objOut;
        }

        public object DeleteNasabah(string id)
        {
            object objOut = new object();
            objOut = _nasabahService.DeleteNasabah(id);

            return objOut;
        }

        public object GetDataKelamin()
        {
            object objOut = new object();
            objOut = _nasabahService.GetDataKelamin();

            return objOut;
        }
        
        public object GetDataProfesi()
        {
            object objOut = new object();
            objOut = _nasabahService.GetDataProfesi();

            return objOut;
        }
    }
}
