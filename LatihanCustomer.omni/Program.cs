using LatihanCustomer.omni;
using LatihanCustomer.omni.Services;
using Microsoft.Extensions.Options;

IConfiguration configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
Action<GlobalVariable> globalVar = (opt =>
{
    opt.BaseBEUrl = configuration.GetSection("CustomerBEUrl").Value;
});

var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins,
                        builder =>
                        {
                            builder.WithOrigins("*")
                                .AllowAnyHeader()
                                .AllowAnyMethod();
                        });
});

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.Configure(globalVar);
builder.Services.AddScoped(resolver => resolver.GetRequiredService<IOptions<GlobalVariable>>().Value);
builder.Services.AddScoped<INasabahService, NasabahService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(MyAllowSpecificOrigins);

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
