using LatihanCustomer.omni.Models;
using Newtonsoft.Json;
using System.Text;

namespace LatihanCustomer.omni.Services
{
    public class NasabahService : INasabahService
    {
        private GlobalVariable _globalVariable;

        public NasabahService(GlobalVariable globalVariable)
        {
            _globalVariable = globalVariable;
        }

        public List<NasabahModel> GetListNasabah()
        {
            List<NasabahModel> output = new List<NasabahModel>();

            HttpClient client = new HttpClient();
            //HttpResponseMessage httpResponse = client.GetAsync($"{_globalVariable.BaseBEUri}/api/Nasabah").Result;
            HttpResponseMessage httpResponse = client.GetAsync($"https://localhost:7201/api/Nasabah").Result;
            string strJSONResponse = httpResponse.Content.ReadAsStringAsync().Result;
            output = JsonConvert.DeserializeObject<List<NasabahModel>>(strJSONResponse);

            Console.WriteLine(output);

            return output;
        }

        public List<ListModel> GetListProfesi() {
            return GetList("Profesi");
        }

        public List<ListModel> GetListJenisKelamin() {
            return GetList("JenisKelamin");
        }

        public List<ListModel> GetListKota() {
            return GetList("DataKota");
        }

        public List<ListModel> GetListKecamatan() {
            return GetList("Kecamatan");
        }

        public List<ListModel> GetListKodePos() {
            return GetList("KodePos");
        }

        public List<ListModel> GetList(string Table)
        {
            List<ListModel> output = new List<ListModel>();

            HttpClient client = new HttpClient();
            //HttpResponseMessage httpResponse = client.GetAsync($"{_globalVariable.BaseBEUri}/api/Nasabah").Result;
            HttpResponseMessage httpResponse = client.GetAsync($"https://localhost:7201/api/Nasabah/"+Table).Result;
            string strJSONResponse = httpResponse.Content.ReadAsStringAsync().Result;
            output = JsonConvert.DeserializeObject<List<ListModel>>(strJSONResponse);

            Console.WriteLine(output);

            return output;
        }

        public object AddNewNasabah(NasabahModel data)
        {
            object output = new object();

            string strJSONRequest = JsonConvert.SerializeObject(data);
            HttpContent content = new StringContent(strJSONRequest, Encoding.UTF8, "application/json");
            HttpClient client = new HttpClient();
            HttpResponseMessage httpResponse = client.PostAsync($"{_globalVariable.BaseBEUrl}/api/Nasabah/Add", content).Result;

            string strJSONResponse = httpResponse.Content.ReadAsStringAsync().Result;
            output = JsonConvert.DeserializeObject(strJSONResponse);
            Console.WriteLine(strJSONResponse);

            return output;
        }

        public object UpdateNasabah(NasabahModel data)
        {
            object output = new object();

            string strJSONRequest = JsonConvert.SerializeObject(data);
            HttpContent content = new StringContent(strJSONRequest, Encoding.UTF8, "application/json");
            HttpClient client = new HttpClient();
            HttpResponseMessage httpResponse = client.PostAsync($"{_globalVariable.BaseBEUrl}/api/Nasabah/Update", content).Result;

            string strJSONResponse = httpResponse.Content.ReadAsStringAsync().Result;
            output = JsonConvert.DeserializeObject(strJSONResponse);
            
            Console.WriteLine(strJSONResponse);

            return output;
        }

        public object DeleteNasabah(string id)
        {
            object output = new object();

            string strJSONRequest = JsonConvert.SerializeObject(id);
            HttpContent content = new StringContent(strJSONRequest, Encoding.UTF8, "application/json");
            HttpClient client = new HttpClient();
            HttpResponseMessage httpResponse = client.PostAsync($"{_globalVariable.BaseBEUrl}/api/Nasabah/Delete", content).Result;

            string strJSONResponse = httpResponse.Content.ReadAsStringAsync().Result;
            output = JsonConvert.DeserializeObject(strJSONResponse);
            Console.WriteLine(strJSONResponse);

            return output;
        }

        public List<CountModel> GetDataKelamin()
        {
            Dictionary<string,int> dict = new Dictionary<string,int>();
            List<NasabahModel> dataNasabah = GetListNasabah();
            List<CountModel> output = new List<CountModel>();
            
            foreach (NasabahModel item in dataNasabah)
            {
                if(!dict.ContainsKey(item.JenisKelamin))
                {
                    dict.Add(
                                          
                        item.JenisKelamin,
                        1);
                    
                }
                else
                {
                    int x = (dict[item.JenisKelamin])+1;
                    dict.Remove(item.JenisKelamin);  
                    dict.Add(
                        item.JenisKelamin,
                        x);                   
                }
                
            };       
            foreach(KeyValuePair<string, int> i in dict){
                output.Add(new CountModel()
                {
                    Value = i.Key,
                    Total = i.Value
                });
            }   
            return output;         
        }

        public List<CountModel> GetDataProfesi()
        {
            Dictionary<string,int> dict = new Dictionary<string,int>();
            List<NasabahModel> dataNasabah = GetListNasabah();
            List<CountModel> output = new List<CountModel>();
            
            foreach (NasabahModel item in dataNasabah)
            {
                if(!dict.ContainsKey(item.Profesi))
                {
                    dict.Add(
                                          
                        item.Profesi,
                        1);
                    
                }
                else
                {
                    int x = (dict[item.Profesi])+1;
                    dict.Remove(item.Profesi);  
                    dict.Add(
                        item.Profesi,
                        x);                   
                }
                
            };       
            foreach(KeyValuePair<string, int> i in dict){
                output.Add(new CountModel()
                {
                    Value = i.Key,
                    Total = i.Value
                });
            }   
            return output;         
        }          
    }
}