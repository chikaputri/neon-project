using LatihanCustomer.omni.Models;

namespace LatihanCustomer.omni.Services
{
    public interface INasabahService
    {
        List<NasabahModel> GetListNasabah();
        List<ListModel> GetListProfesi();
        List<ListModel> GetListJenisKelamin();
        List<ListModel> GetListKecamatan();
        List<ListModel> GetListKota();
        List<ListModel> GetListKodePos();
        object AddNewNasabah(NasabahModel data);
        object UpdateNasabah(NasabahModel data);
        object DeleteNasabah(string id);
        List<CountModel> GetDataKelamin();
        List<CountModel> GetDataProfesi();
    }
}
