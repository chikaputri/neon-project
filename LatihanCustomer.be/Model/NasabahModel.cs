namespace LatihanCustomer.be.Models
{
    public class NasabahModel
    {
        public string CustomerId { get; set; }
        public string Nama { get; set; }
        public string NIK { get; set; }
        public string DOB { get; set; }
        public string JenisKelamin { get; set; }
        public string AlamatDomisili { get; set; }
        public string AlamatKTP { get; set; }
        public string AlamatKantor { get; set; }
        public string EmergencyContact { get; set; }
        public string Periode { get; set; }
        public string Profesi { get; set; }
        public string MonthlyIncome { get; set; }
        public string NamaPerusahaan { get; set; }
    }
}
