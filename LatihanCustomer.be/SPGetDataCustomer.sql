use [SQL_NEON]
go

alter proc [dbo].[getDataCustomer]

	as
	set nocount on

	begin
			select dbo.Customer_TM.CustomerId, Nama, DOB, NIK, JenisKelamin, 
					AlamatDomisili, AlamatKTP, AlamatKantor, EmergencyContact,
					Periode, Profesi, MonthlyIncome, NamaPerusahaan
			from dbo.Customer_TM join
			dbo.CustomerAddress_TR on Customer_TM.CustomerId=CustomerAddress_TR.CustomerId
			join dbo.CustomerJob_TR on CustomerJob_TR.CustomerId=CustomerAddress_TR.CustomerId
			order by dbo.Customer_TM.Nama
	end
