use [SQL_NEON]
go

alter proc [dbo].[addCustomer]
	
	@pcCustomerId varchar(32),
	@pcNama varchar(125),
	@pcDOB varchar(125),
	@pcNIK varchar(125),
	@pcJenisKelamin varchar(125),
	@pcAlamatDomisili varchar(125),
	@pcAlamatKTP varchar(125),
	@pcAlamatKantor varchar(125),
	@pcEmergencyContact varchar(125),
	@pcPeriode varchar(125),
	@pcProfesi varchar(125),
	@pcMonthlyIncome varchar(125),
	@pcNamaPerusahaan varchar(125)

	as
	set nocount on

begin
	begin try
	begin tran
		if not exists (select * from dbo.Customer_TM where CustomerId=@pcCustomerId)
		begin
			insert dbo.Customer_TM (CustomerId,Nama, DOB,NIK,JenisKelamin)
			values(@pcCustomerId, @pcNama,@pcDOB,@pcNIK,@pcJenisKelamin)
		
			insert dbo.CustomerAddress_TR(CustomerId, AlamatDomisili, AlamatKTP, AlamatKantor, EmergencyContact)
			values(@pcCustomerId, @pcAlamatDomisili, @pcAlamatKTP, @pcAlamatKantor, @pcEmergencyContact)
		
			insert dbo.CustomerJob_TR(CustomerId, Periode, Profesi, MonthlyIncome, NamaPerusahaan)
			values(@pcCustomerId, @pcPeriode, @pcProfesi, @pcMonthlyIncome, @pcNamaPerusahaan)
		end
	commit tran
	end try

	begin catch
		rollback tran
		return select error_message() as ErrorMessage
	end catch
end