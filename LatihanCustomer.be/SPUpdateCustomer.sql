use [SQL_NEON]
go

alter proc [dbo].[updateCustomer]
	
	@pcCustomerId varchar(32),
	@pcNama varchar(125),
	@pcDOB varchar(125),
	@pcNIK varchar(125),
	@pcJenisKelamin varchar(125),
	@pcAlamatDomisili varchar(125),
	@pcAlamatKTP varchar(125),
	@pcAlamatKantor varchar(125),
	@pcEmergencyContact varchar(125),
	@pcPeriode varchar(125),
	@pcProfesi varchar(125),
	@pcMonthlyIncome varchar(125),
	@pcNamaPerusahaan varchar(125)

	as
	set nocount on
begin
	begin try
	begin tran
		update dbo.Customer_TM
		set 
			Nama=@Nama,
			DOB=@DOB,
			NIK=@NIK,
			JenisKelamin=@JenisKelamin
		where CustomerId=@CustomerId
		update dbo.CustomerAddress_TR
		set
			AlamatDomisili=@AlamatDomisili,
			AlamatKTP=@AlamatKTP,
			AlamatKantor=@AlamatKantor,
			EmergencyContact=@EmergencyContact
		where CustomerId=@CustomerId
		update dbo.CustomerJob_TR
		set
			Periode=@Periode, 
			Profesi=@Profesi, 
			MonthlyIncome=@MonthlyIncome, 
			NamaPerusahaan=@NamaPerusahaan
		where CustomerId=@CustomerId
	commit tran
	end try

	begin catch
		rollback tran
		return select error_message() as ErrorMessage
	end catch
end