﻿using LatihanCustomer.be.Logic;
using LatihanCustomer.be.Service;
using LatihanCustomer.be.Models;
using Microsoft.AspNetCore.Mvc;

namespace LatihanCustomer.be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NasabahController : ControllerBase
    {
        private readonly GlobalVariable _GlobalVariable;
        private IDbService _DBService;

        public NasabahController(GlobalVariable globalVariable, IDbService dbService)
        {
            this._DBService = dbService;
            this._GlobalVariable = globalVariable;
        }

        [HttpGet]
        public object GetListNasabah()
        {
            object apiResponse = new object();
            Console.WriteLine("Start GetListNasabah");

            try
            {
                using (NasabahCoreLogic nasabahCore = new NasabahCoreLogic(_GlobalVariable, _DBService))
                {
                    apiResponse = nasabahCore.GetListNasabah();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine($"error controller, {ex.Message}");
            }
            Console.WriteLine("End GetListNasabah");

            return apiResponse;
        }

        [HttpGet ("Profesi")]
        public object GetListProfesi()
        {
            object apiResponse = new object();
            Console.WriteLine("Start GetListProfesi");

            try
            {
                using (NasabahCoreLogic nasabahCore = new NasabahCoreLogic(_GlobalVariable, _DBService))
                {
                    apiResponse = nasabahCore.GetListProfesi();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine($"error controller, {ex.Message}");
            }
            Console.WriteLine(" End GetListProfesi");

            return apiResponse;
        }

        [HttpGet ("JenisKelamin")]
        public object GetListJenisKelamin()
        {
            object apiResponse = new object();
            Console.WriteLine("Start GetListJenisKelamin");

            try
            {
                using (NasabahCoreLogic nasabahCore = new NasabahCoreLogic(_GlobalVariable, _DBService))
                {
                    apiResponse = nasabahCore.GetListJenisKelamin();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine($"error controller, {ex.Message}");
            }
            Console.WriteLine(" End GetListJenisKelamin");

            return apiResponse;
        }

        [HttpGet ("Kecamatan")]
        public object GetListKecamatan()
        {
            object apiResponse = new object();
            Console.WriteLine("Start GetListKecamatan");

            try
            {
                using (NasabahCoreLogic nasabahCore = new NasabahCoreLogic(_GlobalVariable, _DBService))
                {
                    apiResponse = nasabahCore.GetListKecamatan();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine($"error controller, {ex.Message}");
            }
            Console.WriteLine(" End GetListKecamatan");

            return apiResponse;
        }

        [HttpGet ("Kota")]
        public object GetListKota()
        {
            object apiResponse = new object();
            Console.WriteLine("Start GetListKota");

            try
            {
                using (NasabahCoreLogic nasabahCore = new NasabahCoreLogic(_GlobalVariable, _DBService))
                {
                    apiResponse = nasabahCore.GetListKota();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine($"error controller, {ex.Message}");
            }
            Console.WriteLine(" End GetListKota");

            return apiResponse;
        }

        [HttpGet ("KodePos")]
        public object GetListKodePos()
        {
            object apiResponse = new object();
            Console.WriteLine("Start GetListKodePos");

            try
            {
                using (NasabahCoreLogic nasabahCore = new NasabahCoreLogic(_GlobalVariable, _DBService))
                {
                    apiResponse = nasabahCore.GetListKodePos();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine($"error controller, {ex.Message}");
            }
            Console.WriteLine(" End GetListKodePos");

            return apiResponse;
        }

        
        [HttpPost ("Add")]
        public object AddNewNasabah([FromBody] NasabahModel data)
        {
            Console.WriteLine("Start AddNewNasabah");
            object apiResponse = new object();

            try
            {
                using (NasabahCoreLogic nasabahCore = new NasabahCoreLogic(_GlobalVariable, _DBService))
                {
                    apiResponse = nasabahCore.AddNewNasabah(data);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"error controller, {ex.Message}");
            }

            Console.WriteLine("End AddNewNasabah");
            return apiResponse;
        }

        [HttpPost ("Update")]
        public object UpdateNasabah([FromBody] NasabahModel data)
        {
            Console.WriteLine("Start UpdateNasabah");
            object apiResponse = new object();

            try
            {
                using (NasabahCoreLogic nasabahCore = new NasabahCoreLogic(_GlobalVariable, _DBService))
                {
                    apiResponse = nasabahCore.UpdateNasabah(data);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"error controller, {ex.Message}");
            }

            Console.WriteLine("End UpdateNasabah");
            return apiResponse;
        }

        [HttpPost ("Delete")]
        public object DeleteNasabah([FromBody] string id)
        {
            Console.WriteLine("Start DeleteNasabah");
            object apiResponse = new object();

            try
            {
                using (NasabahCoreLogic nasabahCore = new NasabahCoreLogic(_GlobalVariable, _DBService))
                {
                    apiResponse = nasabahCore.DeleteNasabah(id);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"error controller, {ex.Message}");
            }

            Console.WriteLine("End DeleteNasabah");
            return apiResponse;
        }
    }
}
