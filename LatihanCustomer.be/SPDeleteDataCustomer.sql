use [SQL_NEON]
go

alter proc [dbo].[deleteCustomer]

	@id varchar(32)

as
begin
		begin try
		begin tran
			delete from dbo.CustomerJob_TR where CustomerId=@id			
			delete from dbo.CustomerAddress_TR where CustomerId=@id
			delete from dbo.Customer_TM where CustomerId=@id
		commit tran
		end try
	begin catch
		rollback tran
		return select error_message() as ErrorMessage
	end catch
end