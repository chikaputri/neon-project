﻿using LatihanCustomer.be.Service;
using LatihanCustomer.be.Models;

namespace LatihanCustomer.be.Logic
{
    public class NasabahCoreLogic : IDisposable
    {
        private GlobalVariable _GlobalVariable;
        private IDbService _dbService;

        public NasabahCoreLogic(GlobalVariable inGlobalVar, IDbService inDbService)
        {
            this._GlobalVariable = inGlobalVar;
            _dbService = inDbService;
        }

        public void Dispose()
        {
            Console.WriteLine("masuk ke dispose");
            
        }

        public object GetListNasabah()
        {
            object objOut = _dbService.GetListNasabah();

            return objOut;
        }

        public object GetListProfesi()
        {
            object objOut = _dbService.GetListProfesi();

            return objOut;
        }

        public object GetListKota()
        {
            object objOut = _dbService.GetListKota();

            return objOut;
        }

        public object GetListKodePos()
        {
            object objOut = _dbService.GetListKodePos();

            return objOut;
        }

        public object GetListKecamatan()
        {
            object objOut = _dbService.GetListKecamatan();

            return objOut;
        }

        public object GetListJenisKelamin()
        {
            object objOut = _dbService.GetListJenisKelamin();

            return objOut;
        }

        public object AddNewNasabah(NasabahModel data)
        {
            object objOut = _dbService.AddNewNasabah(data);

            return objOut;
        }

        public object UpdateNasabah(NasabahModel data)
        {
            object objOut = _dbService.UpdateNasabah(data);

            return objOut;
        }

        public object DeleteNasabah(string id)
        {
            object objOut = _dbService.DeleteNasabah(id);

            return objOut;
        }
    }
}
