use [SQL_NEON]
go

alter proc [dbo].[getCustomerById]

	@pcCustomerId varchar(32)

as
begin
		begin try
		begin tran
			select * from 
			(select dbo.Customer_TM.CustomerId, Nama, DOB, NIK, JenisKelamin, 
					AlamatDomisili, AlamatKTP, AlamatKantor, EmergencyContact,
					Periode, Profesi, MonthlyIncome, NamaPerusahaan
			from dbo.Customer_TM join
			dbo.CustomerAddress_TR on Customer_TM.CustomerId=CustomerAddress_TR.CustomerId
			join dbo.CustomerJob_TR on CustomerJob_TR.CustomerId=CustomerAddress_TR.CustomerId
			) as allCustomer
			where allCustomer.CustomerId=@pcCustomerId
		commit tran
		end try
	begin catch
		rollback tran
		return select error_message() as ErrorMessage
	end catch
end