using LatihanCustomer.be;
using LatihanCustomer.be.Service;
using Microsoft.Extensions.Options;

IConfiguration configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

Action<GlobalVariable> globalVar = (opt =>
{
    opt.ConnectionString = configuration.GetSection("ConnectionStrings").Value;
});


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//ditambah untuk appsetting
builder.Services.Configure(globalVar);
builder.Services.AddScoped(resolver => resolver.GetRequiredService<IOptions<GlobalVariable>>().Value);
builder.Services.AddScoped<IDbService, DbService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
