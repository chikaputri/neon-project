﻿using Microsoft.Extensions.Options;

namespace LatihanCustomer.be
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        public void ConfigureServices (IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_2_1);

            IConfiguration configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();


            Action<GlobalVariable> globalVar = ( opt =>
            {
                opt.ConnectionString = configuration.GetSection("ConnectionStrings").Value;
            });

            services.Configure(globalVar);
            services.AddScoped(resolver => resolver.GetRequiredService<IOptions<GlobalVariable>>().Value);
            services.AddScoped<Service.DbService, Service.DbService>();

        }

        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
