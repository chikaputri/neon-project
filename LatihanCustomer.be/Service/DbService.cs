﻿using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using LatihanCustomer.be.Models;
using Newtonsoft.Json.Linq;

namespace LatihanCustomer.be.Service
{
    public class DbService : IDbService
    {        
        public List<NasabahModel> GetListNasabah()
        {
            List<NasabahModel> returnVal = new List<NasabahModel>();

            string conString = @"Server=(LocalDb)\MSSQLLocalDB;DataBase=SQL_NEON"; 
            SqlConnection conn = new SqlConnection(conString);
           // SqlConnection conn = new SqlConnection(_GlobalVariable.ConnectionString);
            DataSet dsOut = new DataSet();
            SqlCommand command = new SqlCommand("getDataCustomer", conn);
            command.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                dataAdapter.Fill(dsOut);
                //returnVal = JsonConvert.SerializeObject(dsOut);
                foreach (DataRow dr in dsOut.Tables[0].Rows)
                {
                    returnVal.Add(new NasabahModel()
                    {
                        CustomerId = (string)dr[0],
                        Nama = (string)dr[1],
                        NIK = (string)dr[2],
                        DOB = (string)dr[3],
                        JenisKelamin = (string)dr[4],
                        AlamatDomisili = (string)dr[5],
                        AlamatKTP = (string)dr[6],
                        AlamatKantor = (string)dr[7],
                        EmergencyContact = (string)dr[8],
                        Periode = (string)dr[9],
                        Profesi = (string)dr[10],
                        MonthlyIncome = (string)dr[11],
                        NamaPerusahaan = (string)dr[12],
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return returnVal;
        }

        public List<ListModel> GetListProfesi() {
            return GetList("getDataProfesi");
        }

        public List<ListModel> GetListJenisKelamin() {
            return GetList("getDataJenisKelamin");
        }

        public List<ListModel> GetListKota() {
            return GetList("getDataKota");
        }

        public List<ListModel> GetListKecamatan() {
            return GetList("getDataKecamatan");
        }

        public List<ListModel> GetListKodePos() {
            return GetList("getDataKodePos");
        }

        public List<ListModel> GetList(string SP)
        {
            List<ListModel> returnVal = new List<ListModel>();

            string conString = @"Server=(LocalDb)\MSSQLLocalDB;DataBase=SQL_NEON"; 
            SqlConnection conn = new SqlConnection(conString);
           // SqlConnection conn = new SqlConnection(_GlobalVariable.ConnectionString);
            DataSet dsOut = new DataSet();
            SqlCommand command = new SqlCommand(SP, conn);
            command.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                dataAdapter.Fill(dsOut);
                //returnVal = JsonConvert.SerializeObject(dsOut);
                foreach (DataRow dr in dsOut.Tables[0].Rows)
                {
                    returnVal.Add(new ListModel()
                    {
                        ID = (int)dr[0],
                        value = (string)dr[1]
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return returnVal;
        }

        public object AddNewNasabah(NasabahModel data)
        {
            object returnVal = new object();
            
            List<string> jenisKelamin = new List<string>();
            foreach(ListModel item in GetListJenisKelamin()){
                   jenisKelamin.Add(item.value);
            };
            List<string> profesi = new List<string>();
            foreach(ListModel item in GetListProfesi()){
                    profesi.Add(item.value);
            };

            string conString = @"Server=(LocalDb)\MSSQLLocalDB;DataBase=SQL_NEON"; 
            SqlConnection conn = new SqlConnection(conString);
           // SqlConnection conn = new SqlConnection(_GlobalVariable.ConnectionString);
            DataSet dsOut = new DataSet();
            SqlCommand command = new SqlCommand("addCustomer", conn);
            command.CommandType = CommandType.StoredProcedure;

            try
            {
                if(jenisKelamin.Contains(data.JenisKelamin) || profesi.Contains(data.Profesi))
                {                        
                    conn.Open();
                             
                    command.Parameters.AddWithValue("@pcCustomerId", SqlDbType.NVarChar).Value = data.CustomerId;
                    command.Parameters.AddWithValue("@pcNama", SqlDbType.NVarChar).Value = data.Nama;
                    command.Parameters.AddWithValue("@pcDOB", SqlDbType.DateTime).Value = data.DOB;
                    command.Parameters.AddWithValue("@pcNIK", SqlDbType.NVarChar).Value = data.NIK;
                    command.Parameters.AddWithValue("@pcJenisKelamin", SqlDbType.NVarChar).Value = data.JenisKelamin;
                    command.Parameters.AddWithValue("@pcAlamatDomisili", SqlDbType.NVarChar).Value = data.AlamatDomisili;
                    command.Parameters.AddWithValue("@pcAlamatKTP", SqlDbType.NVarChar).Value = data.AlamatKTP;
                    command.Parameters.AddWithValue("@pcAlamatKantor", SqlDbType.NVarChar).Value = data.AlamatKantor;
                    command.Parameters.AddWithValue("@pcEmergencyContact", SqlDbType.NVarChar).Value = data.EmergencyContact;
                    command.Parameters.AddWithValue("@pcPeriode", SqlDbType.NVarChar).Value = data.Periode;
                    command.Parameters.AddWithValue("@pcProfesi", SqlDbType.NVarChar).Value = data.Profesi;
                    command.Parameters.AddWithValue("@pcMonthlyIncome", SqlDbType.NVarChar).Value = data.MonthlyIncome;
                    command.Parameters.AddWithValue("@pcNamaPerusahaan", SqlDbType.NVarChar).Value = data.NamaPerusahaan;
                    command.ExecuteNonQuery();

                    Console.WriteLine("berhasil");
                }
                else
                {
                    if(!jenisKelamin.Contains(data.JenisKelamin))
                    {
                        returnVal = "Data jenisKelamin '"+ data.JenisKelamin + "' tidak ditemukan";
                    }
                    else if(!profesi.Contains(data.Profesi))
                    {
                        returnVal = "Data profesi '"+ data.Profesi + "' tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);                
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return returnVal;
        }

        public object UpdateNasabah(NasabahModel data)
        {
            object returnVal = new object();

            string conString = @"Server=(LocalDb)\MSSQLLocalDB;DataBase=SQL_NEON"; 
            SqlConnection conn = new SqlConnection(conString);
           // SqlConnection conn = new SqlConnection(_GlobalVariable.ConnectionString);
            DataSet dsOut = new DataSet();
            SqlCommand command = new SqlCommand("updateCustomer", conn);
            command.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                command.Parameters.AddWithValue("@pcCustomerId", SqlDbType.NVarChar).Value = data.CustomerId;
                command.Parameters.AddWithValue("@pcNama", SqlDbType.NVarChar).Value = data.Nama;
                command.Parameters.AddWithValue("@pcDOB", SqlDbType.DateTime).Value = data.DOB;
                command.Parameters.AddWithValue("@pcNIK", SqlDbType.NVarChar).Value = data.NIK;
                command.Parameters.AddWithValue("@pcJenisKelamin", SqlDbType.NVarChar).Value = data.JenisKelamin;
                command.Parameters.AddWithValue("@pcAlamatDomisili", SqlDbType.NVarChar).Value = data.AlamatDomisili;
                command.Parameters.AddWithValue("@pcAlamatKTP", SqlDbType.NVarChar).Value = data.AlamatKTP;
                command.Parameters.AddWithValue("@pcAlamatKantor", SqlDbType.NVarChar).Value = data.AlamatKantor;
                command.Parameters.AddWithValue("@pcEmergencyContact", SqlDbType.NVarChar).Value = data.EmergencyContact;
                command.Parameters.AddWithValue("@pcPeriode", SqlDbType.NVarChar).Value = data.Periode;
                command.Parameters.AddWithValue("@pcProfesi", SqlDbType.NVarChar).Value = data.Profesi;
                command.Parameters.AddWithValue("@pcMonthlyIncome", SqlDbType.NVarChar).Value = data.MonthlyIncome;
                command.Parameters.AddWithValue("@pcNamaPerusahaan", SqlDbType.NVarChar).Value = data.NamaPerusahaan;
                command.ExecuteNonQuery();

                Console.WriteLine("berhasil");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);                
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return returnVal;
        }

        public object DeleteNasabah(string id)
        {
            object returnVal = new object();

            string conString = @"Server=(LocalDb)\MSSQLLocalDB;DataBase=SQL_NEON"; 
            SqlConnection conn = new SqlConnection(conString);
           // SqlConnection conn = new SqlConnection(_GlobalVariable.ConnectionString);
            DataSet dsOut = new DataSet();
            SqlCommand command = new SqlCommand("deleteCustomer", conn);
            command.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                command.Parameters.AddWithValue("@pcCustomerId", id);
                command.ExecuteNonQuery();
                
                Console.WriteLine("berhasil");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);                
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return returnVal;
        }
    }
}
