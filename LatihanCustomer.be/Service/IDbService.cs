﻿using LatihanCustomer.be.Models;

namespace LatihanCustomer.be.Service
{
    public interface IDbService
    {
        List<NasabahModel> GetListNasabah();
        List<ListModel> GetListProfesi();
        List<ListModel> GetListJenisKelamin();
        List<ListModel> GetListKecamatan();
        List<ListModel> GetListKota();
        List<ListModel> GetListKodePos();
        object AddNewNasabah(NasabahModel data);
        object UpdateNasabah(NasabahModel data);
        object DeleteNasabah(string id);
    }
}
