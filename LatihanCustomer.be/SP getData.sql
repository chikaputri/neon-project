use [SQL_NEON]
go

alter proc [dbo].[getDataKodePos]

	as
	set nocount on

	begin
			select * from dbo.KodePos
	end


alter proc [dbo].[getDataKota]

	as
	set nocount on

	begin
			select * from dbo.Kota
	end;


alter proc [dbo].[getDataJenisKelamin]

	as
	set nocount on

	begin
			select * from dbo.JenisKelamin
	end;

alter proc [dbo].[getDataKecamatan]

	as
	set nocount on

	begin
			select * from dbo.Kecamatan
	end;

alter proc [dbo].[getDataProfesi]

	as
	set nocount on

	begin
			select * from dbo.Profesi
	end;


