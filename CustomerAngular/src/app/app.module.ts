import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import { LineChartComponent } from './components/line-chart/line-chart.component';
import { BarChartComponent } from './components/bar-chart/bar-chart.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ListNasabahComponent } from './components/list-nasabah/list-nasabah.component';
import { AddNasabahComponent } from './components/add-nasabah/add-nasabah.component';
import { UpdateNasabahComponent } from './components/update-nasabah/update-nasabah.component';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    PieChartComponent,
    LineChartComponent,
    BarChartComponent,
    NavbarComponent,
    ListNasabahComponent,
    AddNasabahComponent,
    UpdateNasabahComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
