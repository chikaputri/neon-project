import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddNasabahComponent } from './components/add-nasabah/add-nasabah.component';
import { ListNasabahComponent } from './components/list-nasabah/list-nasabah.component';
import { UpdateNasabahComponent } from './components/update-nasabah/update-nasabah.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import { BarChartComponent } from './components/bar-chart/bar-chart.component';
import { LineChartComponent } from './components/line-chart/line-chart.component';

const routes: Routes = [
  { path: '', redirectTo: '/list-customer', pathMatch: 'full' },
  { path: 'list-customer', component: ListNasabahComponent },
  { path: 'add-customer', component: AddNasabahComponent },
  { path: 'update-customer', component: UpdateNasabahComponent },
  { path: 'pie-chart', component: PieChartComponent },
  { path: 'bar-chart', component: BarChartComponent },
  { path: 'line-chart', component: LineChartComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
