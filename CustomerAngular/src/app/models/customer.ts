export interface Customer
{
    customerId: String;
	nama: String;
	nik: String;
	dob: String;
	jenisKelamin: String;
	alamatDomisili: String;
	alamatKTP: String;
	alamatKantor: String;
	emergencyContact: String;
	periode: String;
	profesi: String;
	monthlyIncome: String;
	namaPerusahaan: String;
}
