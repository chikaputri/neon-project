import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Customer } from '../models/customer';
import { Count } from '../models/count';
import { GetList } from '../models/getList';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private apiURL = "http://localhost:5148/api"

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }

  getCustomers(): Observable<Customer[]>
  {
    return this.httpClient.get<Customer[]>(this.apiURL+'/Nasabah');
  }

  getKota(): Observable<GetList[]>
  {
    return this.httpClient.get<GetList[]>(this.apiURL+'/Nasabah/Kota');
  }

  getKodePos(): Observable<GetList[]>
  {
    return this.httpClient.get<GetList[]>(this.apiURL+'/Nasabah/KodePos');
  }

  getKecamatan(): Observable<GetList[]>
  {
    return this.httpClient.get<GetList[]>(this.apiURL+'/Nasabah/Kecamatan');
  }

  getProfesi(): Observable<GetList[]>
  {
    return this.httpClient.get<GetList[]>(this.apiURL+'/Nasabah/Profesi');
  }

  getJenisKelamin(): Observable<GetList[]>
  {
    return this.httpClient.get<GetList[]>(this.apiURL+'/Nasabah/JenisKelamin');
  }

  getDataKelamin(): Observable<Count[]>
  {    
    return this.httpClient.get<Count[]>(this.apiURL+'/Nasabah/GetDataKelamin');
  }
  
  getDataProfesi(): Observable<Count[]>
  {    
    return this.httpClient.get<Count[]>(this.apiURL+'/Nasabah/GetDataProfesi');
  }

  deleteCustomer(customerId: String)
  {
    return this.httpClient.post<Customer>(this.apiURL+'/Nasabah/Delete', JSON.stringify(customerId), this.httpOptions);
  }

  updateCustomer(customer: Customer): Observable<Customer>
  {
    return this.httpClient.post<Customer>(this.apiURL + 'Nasabah/Update', JSON.stringify(customer), this.httpOptions);
  }

  addCustomer(customer: Customer): Observable<Customer>
  {
    return this.httpClient.post<Customer>(this.apiURL+'/Nasabah/Add', JSON.stringify(customer), this.httpOptions);
  }
}
