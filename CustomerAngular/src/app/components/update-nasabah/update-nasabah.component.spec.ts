import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateNasabahComponent } from './update-nasabah.component';

describe('UpdateNasabahComponent', () => {
  let component: UpdateNasabahComponent;
  let fixture: ComponentFixture<UpdateNasabahComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateNasabahComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateNasabahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
