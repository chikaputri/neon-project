import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Customer } from 'src/app/models/customer';
import { CustomerService } from 'src/app/service/customer.service';

@Component({
  selector: 'app-update-nasabah',
  templateUrl: './update-nasabah.component.html',
  styleUrls: ['./update-nasabah.component.css']
})
export class UpdateNasabahComponent {

  updateForm;

  constructor(
    public customerService: CustomerService,
    private router: Router
  )
  {
    this.updateForm = new FormGroup ({
      customerId: new FormControl(''),
        nama: new FormControl(''),
        nik: new FormControl(''),
        dob: new FormControl(''),
        jenisKelamin: new FormControl(''),
        alamatDomisili: new FormControl(''),
        alamatKTP:new FormControl(''),
        alamatKantor: new FormControl(''),
        emergencyContact: new FormControl(''),
        periode: new FormControl(''),
        profesi: new FormControl(''),
        monthlyIncome: new FormControl(''),
        namaPerusahaan: new FormControl('')
    });
  
  }

  onSubmit(formData: FormGroup){
    this.customerService.updateCustomer(formData.value).subscribe(res => {
      this.router.navigateByUrl("/list-customer");
    });
  }
}
