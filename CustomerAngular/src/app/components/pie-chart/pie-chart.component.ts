import { Component } from '@angular/core';
import { CustomerService } from 'src/app/service/customer.service';
import { Count } from 'src/app/models/count';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent {

  counts : Count[] = [];
  chartLabels = new Array();
  chartDatasets = new Array();
  value = new Array();
  charName = 'Jenis Kelamin';


  constructor(
    public customerService: CustomerService) { }
  
  ngOnInit(): void {
    this.customerService.getDataKelamin().subscribe((data: Count[]) => {
      this.counts = data;
      console.log(this.counts);

      this.chartDatasets = [
        { data: this.value, label: this.charName }
      ]

      for (let index = 0; index < this.counts.length; index++) {
        this.chartLabels.push(this.counts[index].value);
        this.value.push(this.counts[index].total);
      }
    });
  }
}
