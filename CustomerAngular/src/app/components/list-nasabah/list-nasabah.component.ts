import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/models/customer';
import { CustomerService } from 'src/app/service/customer.service';
import { Observable, throwError } from 'rxjs';
import { UpdateNasabahComponent } from '../update-nasabah/update-nasabah.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list-nasabah',
  templateUrl: './list-nasabah.component.html',
  styleUrls: ['./list-nasabah.component.css']
})
export class ListNasabahComponent {
  customers : Customer[] = [];
  
  constructor(
    public customerService: CustomerService,
    private router: Router
  )
  {  }

  ngOnInit(): void {
    this.customerService.getCustomers().subscribe((data: Customer[]) => {
      this.customers = data;
      console.log(this.customers);
    });
  }

  deleteCustomer(customerId: String) {
    this.customerService.deleteCustomer(customerId).subscribe(res => {
      this.customers = this.customers.filter(item => item.customerId !== customerId);
    });
  }
}

