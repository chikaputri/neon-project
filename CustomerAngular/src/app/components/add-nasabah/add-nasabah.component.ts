import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Customer } from 'src/app/models/customer';
import { GetList } from 'src/app/models/getList';
import { CustomerService } from 'src/app/service/customer.service';

@Component({
  selector: 'app-add-nasabah',
  templateUrl: './add-nasabah.component.html',
  styleUrls: ['./add-nasabah.component.css']
})
export class AddNasabahComponent {

  addForm;

  constructor(
    public customerService: CustomerService,
    private router: Router
  )
  {
    this.addForm = new FormGroup ({
      customerId: new FormControl(''),
        nama: new FormControl(''),
        nik: new FormControl(''),
        dob: new FormControl(''),
        jenisKelamin: new FormControl(''),
        alamatDomisili: new FormControl(''),
        alamatKTP:new FormControl(''),
        alamatKantor: new FormControl(''),
        emergencyContact: new FormControl(''),
        periode: new FormControl(''),
        profesi: new FormControl(''),
        monthlyIncome: new FormControl(''),
        namaPerusahaan: new FormControl('')
    });
  
  }

  
  jenisKelamin : GetList[] = [];
  profesi : GetList[] = [];

  ngOnInit(): void {
    this.customerService.getJenisKelamin().subscribe((data: GetList[]) => {
      this.jenisKelamin = data;
      console.log(this.jenisKelamin);
    });
    this.customerService.getProfesi().subscribe((data: GetList[]) => {
      this.profesi = data;
      console.log(this.profesi);
    });
  }

  onSubmit(formData: FormGroup){
    this.customerService.addCustomer(formData.value).subscribe(res => {
      this.router.navigateByUrl("/list-customer");
    });
  }
}
