import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-form-component',
  templateUrl: './reactive-form-component.component.html',
  styleUrls: ['./reactive-form-component.component.css']
})
export class ReactiveFormComponentComponent {

  curentUser:{
    isLogin:boolean,
    username: string,
    password: string
  } = { isLogin: false, username:'', password:''}

  loginForm = new FormGroup ({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(5)
    ]),
    password: new FormControl('')
  })

  get username(){
    return this.loginForm.get('username')
  }

  onLogin(){
    console.log(this.loginForm);

    this.curentUser = {
      isLogin: true,
      username: this.loginForm.value.username,
      password: this.loginForm.value.password
    }
  }

}
