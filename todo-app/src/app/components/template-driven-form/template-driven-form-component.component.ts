import { Component } from '@angular/core';
import { Task } from 'src/app/models/Task';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-template-driven-form-component',
  templateUrl: './template-driven-form-component.component.html',
  styleUrls: ['./template-driven-form-component.component.css']
})
export class TemplateDrivenFormComponentComponent {

  tasks: Task[] = []
  categories = ['School', 'Work', 'Hobby']

  onSubmit(form:NgForm){
    const { taskName, category } = form.value
    this.tasks = [...this.tasks, new Task(taskName, false, category)]
    form.reset()
  }

}
