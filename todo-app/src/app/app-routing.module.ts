import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TemplateDrivenFormComponentComponent } from './components/template-driven-form/template-driven-form-component.component';
import { ReactiveFormComponentComponent } from './components/reactive-form/reactive-form-component.component';
import { NoPageFoundComponent } from './components/no-page-found/no-page-found.component';

import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';

import { AuthGuard } from './service/auth.guard';
import { UserProfileComponent } from './components/user-profile/user-profile.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: SigninComponent },
  { path: 'register', component: SignupComponent },
  { path: 'user-profile/:id', component: UserProfileComponent, canActivate: [AuthGuard] },
  { path: 'todo-list', component: TodoListComponent },
  { path: 'template-driven', component: TemplateDrivenFormComponentComponent },
  { path: 'reactive-form', component: ReactiveFormComponentComponent },
  { path: '**', component: NoPageFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
