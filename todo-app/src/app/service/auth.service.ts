import { Injectable } from '@angular/core';
import { User } from '../models/User';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  endpoint: string = "https://laksanajaya.apdis.fun/api";
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  currentUser: {name: string, email: string, _id: string} = {name: '', email: '', _id: ''};

  constructor(private http: HttpClient, private router: Router) { }

  //Sign-up
  signUp(user: User): Observable<any> {
    let api = `${this.endpoint}/register-user`;
    return this.http
      .post(api, user)
      .pipe( catchError(this.handleError) );
  }

  //Sign-in
  signIn(user: User) {
    let api = `${this.endpoint}/login`;
    return this.http
      .post(api, user)
      .subscribe((res: any) => {
        localStorage.setItem('accessToken', res.data.accessToken);
        this.getUserProfile(res.data.id)
          .subscribe((res: any) => {
            this.currentUser = res.data;
            this.router.navigate(['user-profile/' + res.data._id]);
          })
      })
  }

  //User profile
  getUserProfile(id: any): Observable<any> {
    let api = `${this.endpoint}/user-profile/?id=${id}`;
    return this.http
      .get(api, {headers: this.headers})
      .pipe(
        map((res: any) => {
          return res || {}
        }),
        catchError(this.handleError)
      )
  }

  //Error handling
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if(error.error instanceof ErrorEvent) {
      //client-side error
      msg = error.error.message;
    } else {
      //server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    return throwError(msg);
  }

  getToken() {
    return localStorage.getItem("accessToken");
  }

  get isLoggedIn(): boolean {
    const authToken = localStorage.getItem('accessToken');
    return (authToken !== null) ? true : false;
  }
}
